"""Library to convert SBML models to symbolic math (sympy) and back.

This module makes it easy to work with kinetic models saved as SBML by
allowing you to directly work with the equations as symbolic math.

In addition, sympy code generation can be used to generate (optimized) code
in several languages to simulate the model.

Note that not all SBML features are currently supported.

Author: Rik van Rosmalen
"""
import setuptools


def readme():
    """Read readme from README.md."""
    with open("README.md", "r") as fh:
        return fh.read()


setuptools.setup(
    name="symbolicSBML",
    version="0.0.1",
    author="Rik van Rosmalen",
    author_email="rikpetervanrosmalen@gmail.com",
    description="Convert ODE models described in SBML format to symbolic math or code.",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/wurssb/Modelling/symbolicsbml",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
    ],
    install_requires=[
        'numpy',
        'sympy',
        'python-libsbml',
    ],
    extras_requires={
        'cython': ["cython"]
    }
)
