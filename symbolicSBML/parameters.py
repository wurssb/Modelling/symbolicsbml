"""Convenience object for referring to parameters.

Contains:
    - Parameters:
        - All parameters from the modular rate law family.
        - Including derived and merged version of these parameters.
        - Global state parameters (R / T).
    - Parameter groupings as sets:
        - base / derived
        - log / nonlog
        - normal / lognormal distributed
        - specific to reaction / metabolite / both / enzyme
        - parameterization schemes (cat / weg / hal)
        - positive / real parameter groups (for assumptions when using symbolics)
    - Parameter mappings:
        - Full names
        - Units
        - SBO Terms
        - LaTeX formatted names/units
        - Matplotlib style LateX formatted names/units
        - Class methods to map parameters to other properties.
    - Class method to calculate the dependencies of each parameter at fixed {T, R}.
    - Standard string separator for combining and splitting parameter names in SBML models.
    - Class method to create symbolic parameters with the right assumptions.

TODO:
    - Expand dependency scheme.
    - Add common parameters of other rate laws as needed.

Author: Rik van Rosmalen
"""
import sympy


class Parameters:
    """Grouping/enumeration for all parameter names.

    This object is not used for anything by itself, but only as an easy way to refer
    to a certain parameter type or grouping of types in a consistent way.

    For example: refer to the km as Parameters.km, while the long name
    "Michaelis constant" can be referred to as Parameters.long.km.

    The units can be found in Parameters.unit.x

    Certain groups are also defined such as base parameters and derived parameters
    (According to Lubitz' parameter balancing scheme).
    See also:
        [1] Lubitz, T., Schulz, M., Klipp, E., & Liebermeister, W. (2010).
            Parameter Balancing in Kinetic Models of Cell Metabolism.
            The Journal of Physical Chemistry B, 114(49), 16298–16303.
            https://doi.org/10.1021/jp108764b
        [2] Liebermeister, W., Uhlendorf, J., & Klipp, E. (2010).
            Modular rate laws for enzymatic reactions: thermodynamics, elasticities and implementation.
            Bioinformatics, 26(12), 1528–1534.
            https://doi.org/10.1093/bioinformatics/btq141
        [3] Lubitz, T., & Liebermeister, W. (2018).
            Parameter balancing: consistent parameter sets for kinetic metabolic models. Bioinformatics.
            https://doi.org/10.1093/bioinformatics/btz129

    TODO: Add extra parameters from and dependencies [3]
    """

    # Base
    km = "km"
    mu = "mu"
    c = "c"
    kv = "kv"
    u = "u"
    base = {km, mu, c, kv, u}

    # Derived
    keq = "keq"
    kcat_prod = kcat_minus = kcat_min = "kcat_prod"
    kcat_sub = kcat_plus = "kcat_sub"
    vmax = vmax_prod = "vmax_prod"
    vmax_sub = "vmax_sub"
    A = "A"
    mu_p = "mu_p"
    derived = {keq, kcat_prod, kcat_sub, vmax_prod, vmax_sub, A, mu_p}

    # Combined (See Section A.3 of the Appendix of [2])
    km_prod = "km_prod"
    km_sub = "km_sub"
    km_tot = "km_tot"
    km_prod_rate = "km_prod_rate"
    km_sub_rate = "km_sub_rate"
    km_tot_rate = "km_tot_rate"
    u_v = "u_v"
    combined = {km_prod, km_sub, km_tot, km_prod_rate, km_sub_rate, km_tot_rate, u_v}

    # Modifiers
    ki = "ki"
    ka = "ka"
    h = cooperativity_factor = "h"
    modifiers = {ki, ka, h}

    # Global state parameters
    R = "R"
    T = "T"
    global_state = {R, T}

    all_balanced_parameters = base | derived | modifiers
    all_parameters = all_balanced_parameters | global_state | combined

    # Categorized by type of distribution
    log = log_normal_distributed = {
        km,
        c,
        kv,
        u,
        keq,
        kcat_prod,
        kcat_sub,
        vmax_prod,
        vmax_sub,
        ki,
        ka,
    } | combined
    lin = non_log = normal_distributed = {mu, mu_p, A, R, T}

    # Assumptions (for sympy)
    real = all_parameters
    # Technically temperature could be 0, but it would not be a metabolic system...
    positive = log | global_state

    # Specificity
    specific_to_enzyme = {
        kcat_prod,
        kcat_sub,
        kv,
        vmax_prod,
        vmax_sub,
        km,
        ki,
        ka,
        u,
        km_prod,
        km_sub,
        km_tot,
        km_prod_rate,
        km_sub_rate,
        km_tot_rate,
        u_v,
        h,
    }
    specific_to_reaction = {keq, A} | specific_to_enzyme
    specific_to_metabolite = {mu, mu_p, km, ki, ka, c}
    specific_to_both = specific_to_metabolite & specific_to_reaction

    # Type of parameters (Thermodynamic versus enzyme kinetic)
    thermodynamic = {mu, mu_p, c, keq, A, R, T}
    enzymatic = all_parameters - thermodynamic

    # If required to create a name for a specific reaction / metabolite /
    # parameter combination, use this separator.
    separator = sep = "__"

    # Overview of required set of parameters for a parameterization
    cat = {u, km, kcat_prod, kcat_sub, c}
    hal = {u, km, kv, keq, c}
    weg = {u, km, kv, mu, R, T, c}

    class Long(object):
        """Grouping/enumeration for all fully written parameter names."""

        display_name = "full name"

        km = "Michaelis constant"
        ki = "inhibitory constant"
        ka = "activation constant"
        h = "reaction cooperativity"
        keq = "equilibrium constant"
        kv = "catalytic rate constant geometric mean"
        u = "concentration of enzyme"
        kcat_prod = "product catalytic rate constant"
        kcat_sub = "substrate catalytic rate constant"
        vmax_prod = "forward maximal velocity"
        vmax_sub = "reverse maximal velocity"
        A = "reaction affinity"
        mu = "standard chemical potential"
        mu_p = "chemical potential"
        c = "concentration"
        R = "gas constant"
        T = "temperature"
        km_prod = "product Michealis constant"
        km_sub = "substrate Michealis constant"
        km_tot = "Total Michealis constant"
        km_prod_rate = "effective forward rate constant"
        km_sub_rate = "effective backward rate constant"
        km_tot_rate = "effective rate constant"
        u_v = "geometric mean of limiting maximal velocities"

    class Unit(object):
        """Grouping/enumeration for all parameter units."""

        display_name = "unit"
        # For some parameters, the units can change depending on the stoichiometry
        # Can also potentially be considered unitless if expressed using standard
        # concentrations (c0 = 1 mM)

        # Note that we define vmax/u_v using the enzyme concentration and not the ammount.
        km = ki = ka = u = c = "mM"
        km_prod = km_sub = km_tot = keq = "mM^n"

        kv = kcat_prod = kcat_sub = "1/s"
        vmax_prod = vmax_sub = u_v = "mM/s"
        km_prod_rate = km_sub_rate = km_tot_rate = "mM^n/s"

        A = mu = mu_p = "kJ/mol"
        T = "K"
        R = "kJ/(mol * K)"  # Note that 8.314 is in kJ/(millimol * K)!

        h = "-"

        stoichiometry_dependent = {
            keq,
            km_prod,
            km_sub,
            km_tot,
            km_prod_rate,
            km_sub_rate,
            km_tot_rate,
        }
        unitless = {h}

    class SBO(object):
        """Grouping/enumeration for all parameter SBO terms."""

        display_name = "SBO term"

        km = "SBO:0000027"
        ki = "SBO:0000261"
        ka = "SBO:0000363"
        h = "SBO:0000382"
        keq = "SBO:0000281"
        kv = "SBO:0000482"
        u = "SBO:0000505"
        kcat_prod = "SBO:0000320"
        kcat_sub = "SBO:0000321"
        vmax_prod = "SBO:0000324"
        vmax_sub = "SBO:0000325"
        A = "SBO:0000618"
        mu = "SBO:0000463"
        mu_p = "SBO:0000303"
        c = "SBO:0000196"
        T = "SBO:0000147"
        R = "SBO:0000567"
        km_prod = "SBO:0000479"
        km_sub = "SBO:0000480"
        km_tot = "SBO:0000478"
        km_prod_rate = "SBO:0000321"
        km_sub_rate = "SBO:0000320"
        km_tot_rate = "SBO:0000025"  # Better alternative?
        u_v = "SBO:0000324"

    class Latex(object):
        """Grouping/enumeration for parameters/units formatted for LaTeX."""

        class Parameter(object):
            """Grouping/enumeration for parameters formatted for LaTeX."""

            display_name = "LateX formatted name"

            km = r"$k^{M}$"
            ki = r"$k^{I}$"
            ka = r"$k^{A}$"
            h = r"$h$"
            keq = r"$k^{eq}$"
            kv = r"$k^{V}$"
            u = r"$u$"
            kcat_prod = r"$k^{cat\texttt{+}}$"
            kcat_sub = r"$k^{cat\texttt{-}}$"
            vmax_prod = r"$v^{max\texttt{+}}$"
            vmax_sub = r"$v^{max\texttt{-}}$"
            A = r"$A$"
            mu = r"$\mu'$"
            mu_p = r"$\mu'^{\circ}$"
            c = r"$c$"
            T = r"$T$"
            R = r"$R$"
            km_prod = r"$k^{M\texttt{+}}$"
            km_sub = r"$k^{M}\texttt{-}$"
            km_tot = r"$k^{M}\texttt{\pm}$"
            km_prod_rate = r"$k^{\texttt{+}}$"
            km_sub_rate = r"$k^{\texttt{-}}$"
            km_tot_rate = r"$k$"
            u_v = r"$v^{V}$"

        class Unit(object):
            """Grouping/enumeration for units formatted for LaTeX."""

            display_name = "LateX formatted unit"

            km = ki = ka = u = c = r"$mM$"
            h = r"$\textnormal{-}$"  # Could also be 1
            kv = kcat_prod = kcat_sub = r"$s^{-1}$"
            vmax_prod = vmax_sub = u_v = r"$mM \cdot s^{-1}$"
            A = mu = mu_p = _energy = r"$kJ \cdot mol^{-1}$"
            km_prod = km_sub = km_tot = r"$mM^{n}$"
            km_prod_rate = km_sub_rate = km_tot_rate = r"$mM^{n}) \cdot s^{-1}$"
            keq = r"$mM^{\Delta n}$"
            T = r"$K$"
            R = r"$kJ \cdot mol^{-1} \cdot K^{-1}$"

    @classmethod
    def split(cls, parameter_string, missing_value=None):
        """Split a parameter string into it's type, metabolite and reaction."""
        ptype, *m_r = parameter_string.split(cls.sep)
        if ptype not in cls.all_parameters:
            raise ValueError("Unknown parameter type: {}".format(ptype))
        elif ptype in cls.specific_to_both:
            metabolite, reaction = m_r
        elif ptype in cls.specific_to_metabolite:
            metabolite, reaction = m_r[0], missing_value
        elif ptype in cls.specific_to_reaction:
            metabolite, reaction = missing_value, m_r[0]
        else:
            metabolite, reaction = missing_value, missing_value
        return ptype, metabolite, reaction

    @classmethod
    def join(cls, ptype, metabolite=None, reaction=None):
        """Combine a parameter type, metabolite and reaction into a standardized string."""
        return cls.sep.join(
            (i for i in (ptype, metabolite, reaction) if i and str(i) != "nan")
        )

    @classmethod
    def as_symbol(cls, parameter, metabolite=None, reaction=None, set_assumptions=True):
        """Create a sympy Symbol for the parameter.

        If set_assumptions is True, the symbol will be marked as real and/or positive
        depending on the parameter type.
        If the reaction or metabolite is passed but not needed it will be ignored.
        """
        if parameter not in cls.all_parameters:
            raise ValueError("Not a valid parameter.")
        # Create the name
        name = [parameter]
        if parameter in cls.specific_to_metabolite:
            if metabolite is None:
                raise ValueError("Metabolite ID required for {}".format(parameter))
            name.append(metabolite)
        if parameter in cls.specific_to_reaction:
            if reaction is None:
                raise ValueError("Reaction ID required for {}".format(parameter))
            name.append(reaction)
        name = cls.separator.join(name)

        # Create the symbolic value
        if set_assumptions:
            return sympy.Symbol(
                name, real=parameter in cls.real, positive=parameter in cls.positive
            )
        return sympy.Symbol(name)

    @classmethod
    def to_latex(
        cls,
        name=None,
        parameter=None,
        metabolite=None,
        reaction=None,
        include=("parameter", "reaction", "metabolite", "unit"),
        matplotlib=True,
    ):
        """Map parameters to their LaTeX representation.

        Convenience function to combine parameters, reaction and/or metabolite
        specifiers and units. Optionally maps to matplotlib tex dialect for ease
        of plotting.

        Input should be either a name that will be split into parts with
        Parameters.split, or all the individual parts.
        """
        specifier_style = "_{{{r},{m}}}"
        style = "${p}{specifier} ({u})$"

        if name is not None:
            parameter, metabolite, reaction = cls.split(name)

        p = cls.as_latex_name(parameter).strip("$") if "parameter" in include else ""
        u = cls.as_latex_unit(parameter).strip("$") if "unit" in include else ""
        r = reaction if reaction is not None and "reaction" in include else ""
        m = metabolite if metabolite is not None and "metabolite" in include else ""

        # If we don't have both r and m, remove the extra comma.
        if not (m and r):
            specifier = specifier_style.replace(",", "").format(m=m, r=r)
        else:
            specifier = specifier_style.format(m=m, r=r)

        # If we don't have u, remove the last unit part.
        if not u:
            style = style.replace(" ({u})", "")

        # Using format_map, we can have redundant keys.
        latex = style.format_map(
            {"p": p, "u": u, "r": r, "m": m, "specifier": specifier}
        )

        # Matplotlibs mathtext does not have \texttt, but uses \mathtt instead.
        # If you use tex=true in matplotlib, this should be set to false!
        if matplotlib:
            latex.replace(r"\texttt", r"\mathtt")
        return latex

    @classmethod
    def dependencies(cls, R, T):
        """Generate the parametric dependencies.

        Returns a nested dictionary: {parameter_type: {dependent_type: (value, stoichiometry))}}
        where value is a constant factor and stoichiometry is True or False meaning that it
        should or shouldn't be multiplied with the stoichiometry of the metabolite in the reaction.
        Note: For parameters in Parameters.thermodynamic this means n, for parameters in
              Parameters.enzymatic this means n * h.
        """
        dependencies = {
            cls.c: {cls.c: (1.0, False),},
            cls.km: {cls.km: (1.0, False),},
            cls.vmax_prod: {
                cls.mu: (-1.0 / (2.0 * R * T), True),
                cls.kv: (1.0, False),
                cls.km: (-1.0 / 2.0, True),
                cls.u: (1.0, False),
            },
            cls.vmax_sub: {
                cls.mu: (1.0 / (2.0 * R * T), True),
                cls.kv: (1.0, False),
                cls.km: (1.0 / 2.0, True),
                cls.u: (1.0, False),
            },
            cls.kcat_prod: {
                cls.mu: (-1.0 / (2.0 * R * T), True),
                cls.kv: (1.0, False),
                cls.km: (-1.0 / 2.0, True),
            },
            cls.kcat_sub: {
                cls.mu: (1.0 / (2.0 * R * T), True),
                cls.kv: (1.0, False),
                cls.km: (1.0 / 2.0, True),
            },
            cls.keq: {cls.mu: (-1.0 / (R * T), True),},
            cls.u: {cls.u: (1.0, False),},
            cls.kv: {cls.kv: (1.0, False),},
            cls.mu: {cls.mu: (1.0, False),},
            cls.mu_p: {cls.mu: (1.0, False), cls.c: (R * T, False),},
            cls.A: {cls.mu: (-1.0, True), cls.c: (-R * T, True),},
            cls.ka: {cls.ka: (1.0, False),},
            cls.ki: {cls.ki: (1.0, False),},
        }
        return dependencies

    @classmethod
    def _subclass_accessor(cls, subclass):
        """Helper to automatically create retrieval functions for the derived classes.

        This saves us from manually adding the mappings.
        Note: Must be instantiated as a classmethod to work best.
        """

        def helper(cls, parameter):
            """Retrieve the {} of the parameter."""
            if parameter in cls.all_parameters:
                return subclass.__dict__[parameter]
            raise ValueError("Not a valid parameter: {}.".format(parameter))
        helper.__doc__ = helper.__doc__.format(subclass.display_name)
        return helper


# Create the convenience accessors (Can't do this inside the class as classmethod won't work!)
Parameters.as_long = classmethod(Parameters._subclass_accessor(Parameters.Long))
Parameters.as_unit = classmethod(Parameters._subclass_accessor(Parameters.Unit))
Parameters.as_sbo = classmethod(Parameters._subclass_accessor(Parameters.SBO))
Parameters.as_latex_name = classmethod(
    Parameters._subclass_accessor(Parameters.Latex.Parameter)
)
Parameters.as_latex_unit = classmethod(
    Parameters._subclass_accessor(Parameters.Latex.Unit)
)
