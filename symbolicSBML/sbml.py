"""Class that directly deals with the SBML code.

Extracts metabolites, reactions and their kinetics and parameter values.

Not all of SBML is supported. Generally supported are:
    - reactions (and their kinetic laws)
    - metabolites (with concentrations, amounts are not supported)
    - observables (metabolites annotated with SBO:0000406: Observable - An entity that
      can be measured quantitatively)
      Note that observables are assumed not to be "physical" species in the model but abstract
      representations of what can be measured (which can be a function of metabolites and other
      parameters)
    - Assignment rules (only for observables)
    - global and local parameters
      Note that local parameters are not used when writing, but will be combined with the
      reaction id for uniqueness and saved as global parameters.
    - Both reading and writing SBML from Sympy expressions are supported
    - Removing reactions or metabolites

Unsupported are (not an exhaustive list):
    - Annotation
    - Types
    - Units
    - Compartments
    - Rules (Algebraic / Rate)
    - Events
    - Flux balance (Constraints / Objectives)
    - Layouts
    - Adding reactions or metabolites (can be done but manually update all relevant terms)

Regarding the mathematical expressions, the following is not supported:
    - Functions/Lambda expressions:
        They are parsed by Sympy, but otherwise not supported in any useful way.
        If they exist in the model, they are automatically replaced using the
        libSBML SBMLFunctionDefinitionConverter
    - Piecewise functions:
        Could be added, but requires some extra work to parse correctly.
    - Function delay
Note that not all mathematical expressions are exhaustively tested.

Author: Rik van Rosmalen
"""
from __future__ import division, print_function

import pathlib
import collections
import itertools
import operator
import sys
import warnings

import numpy as np
import sympy

import libsbml

PYTHON_2 = sys.version_info[0] == 2


class SBMLModel(object):
    def __init__(
        self,
        path,
        ignore_reactions=None,
        ignore_metabolites=None,
        ignore_exchanges=False,
    ):
        """Extract the structure and rate-law equations from an SBML model.

        :param path: filepath
        :type path: str
        :param ignore_reactions: Any reactions that should not be included in
        the flux terms or stoichiometry? Useful when reading in models meant
        for flux balance analysis.
        :param ignore_metabolites: Any metabolites that should not be included
        when reading in the model.
        :type ignore_metabolites: set[str]
        :type ignore_reactions: set[str]
        :param ignore_exchanges: Ignore any exchange reactions, defaults to False.
        :type ignore_exchanges: bool
        """
        if ignore_reactions is None:
            ignore_reactions = set()
        if ignore_metabolites is None:
            ignore_metabolites = set()

        self.ignore_reactions = ignore_reactions
        self.ignore_metabolites = ignore_metabolites
        self.ignore_exchanges = ignore_exchanges

        path = pathlib.Path(path)
        if not (path.exists() and path.is_file()):
            raise FileNotFoundError

        self.document = libsbml.readSBML(str(path.resolve()))

        if self.document.getNumErrors() > 0:
            self.document.printErrors()
            raise ValueError("Errors found in SBML file: {}".format(path))

        self.model = self.document.getModel()

        # Replace any function definitions using this handy libsbml converter.
        if self.model.getNumFunctionDefinitions() > 0:
            config = libsbml.ConversionProperties()
            config.addOption("expandFunctionDefinitions")
            status = self.document.convert(config)
            if status != libsbml.LIBSBML_OPERATION_SUCCESS:
                raise ValueError("Could not convert function defintitions.")

        # These fields will be set in the helper extract methods.
        self.reactions = []
        self.metabolites = []
        self.stoichiometry = np.array([])
        self.parameters = []
        self._local_parameters = {}
        self.fluxes = collections.OrderedDict()
        self.initial_concentrations = collections.OrderedDict()
        self.parameter_values = collections.OrderedDict()
        self.metabolite_index = {}
        self.reaction_index = {}
        self.observables = []
        self.observable_terms = collections.OrderedDict()
        self.assignments = collections.OrderedDict()

        self._extract_structure()
        self._extract_assignment_rules()
        self._extract_flux_terms()
        self._extract_parameter_values()
        self._extract_observables()

    def _reindex(self):
        """Re-index the metabolites and reactions."""
        self.metabolite_index = {j: i for i, j in enumerate(self.metabolites)}
        self.reaction_index = {j: i for i, j in enumerate(self.reactions)}

    @classmethod
    def from_structure(cls, metabolites, reactions, stoichiometry):
        """Create a new SBML model.

        :param metabolites: List of metabolite names
        :type metabolites: list[str]
        :param reactions: List of reaction names
        :type reactions: list[str]
        :param stoichiometry: Stoichiometry matrix
        :type stoichiometry: (M, R) array
        """
        self = cls.__new__(cls)
        self.model = None
        self.ignore_reactions = set()
        self.ignore_exchanges = False

        self.reactions = reactions
        self.metabolites = metabolites
        self.stoichiometry = np.array(stoichiometry)
        if self.stoichiometry.shape != (len(self.metabolites), len(self.reactions)):
            raise ValueError(
                "Stoichiometry does not match metabolite ({})"
                " / reaction shape ({}): {}".format(
                    len(self.metabolites), len(self.reactions), self.stoichiometry.shape
                )
            )
        self._reindex()

        self.parameters = []
        self._local_parameters = {}

        self.fluxes = collections.OrderedDict()
        self.initial_concentrations = collections.OrderedDict()
        self.parameter_values = collections.OrderedDict()
        self.observables = []
        self.observable_terms = collections.OrderedDict()

        return self

    def _extract_structure(self):
        """Extract the reactions, metabolites and stoichiometry from an SBML model."""
        # Get metabolites (known as species in SBML) - skip anything marked as an observable.
        # Observables are marked as using:
        #   SBO:0000406 (Observable - An entity that can be measured quantitatively)
        # There isn't really anything more appropriate way to encode what can be measured
        # in the model. (Bit of a hack, but similar to what AMICI does.)
        self.metabolites = [
            m.getId()
            for m in self.model.getListOfSpecies()
            if (
                m.getSBOTermID() != "SBO:0000406"
                and m.getId() not in self.ignore_metabolites
            )
        ]

        self.observables = [
            m.getId()
            for m in self.model.getListOfSpecies()
            if (
                m.getSBOTermID() == "SBO:0000406"
                and m.getId() not in self.ignore_metabolites
            )
        ]

        # Create stoichiometry matrix
        S = np.zeros((len(self.metabolites), self.model.getNumReactions()))
        index = {j: i for i, j in enumerate(self.metabolites)}

        # Get reactions
        reactions = []
        to_keep = []
        for i, r in enumerate(self.model.getListOfReactions()):
            r_id = r.getId()
            if r_id in self.ignore_reactions:
                continue
            if self.ignore_exchanges and (
                r.getNumProducts() == 0 or r.getNumReactants() == 0
            ):
                continue
            reactions.append(r_id)
            to_keep.append(i)

            # Fill up stoichiometry terms
            for m in r.getListOfProducts():
                try:
                    S[index[m.getSpecies()], i] = m.getStoichiometry()
                except KeyError:
                    if m.getSpecies() in self.ignore_metabolites:
                        pass
                    else:
                        raise
            for m in r.getListOfReactants():
                try:
                    S[index[m.getSpecies()], i] = -m.getStoichiometry()
                except KeyError:
                    if m.getSpecies() in self.ignore_metabolites:
                        pass
                    else:
                        raise

        self.reactions = reactions
        self.stoichiometry = S[:, to_keep]
        self._reindex()

    def _extract_assignment_rules(self):
        for rule in self.model.getListOfRules():
            if not rule.isAssignment():
                raise ValueError("Only assignment rules are currently supported.")
            variable = rule.getVariable()
            # These are treated special.
            if variable in self.observables:
                continue
            variable = sympy.Symbol(variable)
            term = ASTNodeWrapper(rule.getMath()).to_sympy()

            self.assignments[variable] = term

        # Check for nested rules, we don't support them since they have to be ordered corretly.
        # libsbml has a converter to get the order if it should ever be useful to implement.
        for _, term in self.assignments.items():
            if any(symbol in self.assignments for symbol in term.free_symbols):
                raise ValueError("Nested assignment rules are currently not supported.")

    def _extract_flux_terms(self):
        """Extract the flux terms and parameters from an SBML model."""
        for r in self.model.getListOfReactions():
            r_id = r.getId()
            if r_id in self.ignore_reactions:
                continue
            if not r.isSetKineticLaw():
                self.fluxes[r_id] = 0
                continue
            law = r.getKineticLaw()
            if not law.isSetMath():
                self.fluxes[r_id] = 0
                continue
            flux_term = ASTNodeWrapper(law.getMath()).to_sympy()
            # Replace any assignment variables in the flux definition.
            flux_term = flux_term.subs(self.assignments)
            self.fluxes[r_id] = flux_term

    def _extract_parameter_values(self, default_value=0.0):
        """Extract the parameter values and initial concentrations.

        :param default_value: Value that will be used if the concentration or value is not set,
        defaults to 0.
        :type default_value: float
        """
        for m in self.model.getListOfSpecies():
            m_id = m.getId()
            if m.isSetInitialConcentration():
                concentration = m.getInitialConcentration()
            else:
                concentration = default_value
            self.initial_concentrations[m_id] = concentration

        # Extract global parameters and their values
        for p in self.model.getListOfParameters():
            p_id = p.getId()
            self.parameters.append(p_id)
            if p.isSetValue():
                value = p.getValue()
            else:
                value = default_value
            self.parameter_values[p_id] = value

        # Extract local parameters
        # self._local_parameters -> mapping of {reaction: old_name: new_name}
        for r in self.model.getListOfReactions():
            r_id = r.getId()
            if r_id in self.ignore_reactions or not r.isSetKineticLaw():
                continue
            law = r.getKineticLaw()
            local_parameters = law.getListOfLocalParameters()
            parameters = law.getListOfParameters()
            if not local_parameters and not parameters:
                continue

            replacements = {}
            for p in itertools.chain(local_parameters, parameters):
                p_id = p.getId()
                name = "_".join((r_id, p_id))
                self._local_parameters[(r_id, p_id)] = name
                self.parameters.append(name)
                replacements[sympy.Symbol(p_id)] = sympy.Symbol(name)
                if p.isSetValue():
                    value = p.getValue()
                else:
                    value = default_value
                self.parameter_values[name] = value

            # Update flux terms with new name(s)
            # Make sure to sympify, as it could be just a number
            self.fluxes[r_id] = sympy.sympify(self.fluxes[r_id]).xreplace(replacements)

    def _extract_observables(self):
        """Extract assignment rules and add them to the model as observables."""
        for rule in self.model.getListOfRules():
            # Currently we do not deal with rate rules or algebraic rules
            if not rule.isAssignment():
                raise ValueError("Only assignment rules are currently supported.")
            variable = rule.getVariable()
            if variable in self.metabolites:
                continue
                # raise ValueError(
                #     "Cannot have an assignment rule for a variable defined as a "
                #     "metabolite species not marked as observable."
                # )
            term = ASTNodeWrapper(rule.getMath()).to_sympy()
            # Replace any assignment variables in the observable definition.
            term = term.subs(self.assignments)
            self.observable_terms[variable] = term

        # Assign all observables without a rule as themselves.
        for observable in self.observables:
            if observable not in self.observable_terms:
                self.observable_terms[variable] = sympy.sympify(variable)

    def copy(self):
        """Create a deep copy of the SBML model."""
        # Create base model.
        new = self.from_structure(
            self.metabolites.copy(), self.reactions.copy(), self.stoichiometry.copy()
        )

        # Stuff that can be copied directly
        new.ignore_exchanges = self.ignore_exchanges

        # Stuff that needs to be shallowed copied (non-nested basic values (str, int))
        new.ignore_reactions = self.ignore_reactions.copy()
        new.ignore_metabolites = self.ignore_metabolites.copy()
        new.parameters = self.parameters.copy()
        new._local_parameters = self._local_parameters.copy()
        new.initial_concentrations = self.initial_concentrations.copy()
        new.parameter_values = self.parameter_values.copy()
        new.observables = self.observables.copy()
        # Sympy expressions are always immutable and can be shallow copied as well.
        new.fluxes = self.fluxes.copy()
        new.observable_terms = self.observable_terms.copy()

        # Stuff that needs to be deep copied.
        new.model = self.model.clone()

        return new

    def participating_metabolites(self, reaction, by_name=True):
        """Retrieve all metabolites and their stoichiometry of a reaction.

        :param reaction: The reaction of which you want to retrieve the metabolites.
        :type reaction: str
        :param by_name: Whether to return metabolites by name or index.
        :type by_name: bool

        :returns: A list of metabolite, stoichiometry tuples.
        :rtype: list[(metabolite, n)]
        """
        if not isinstance(reaction, int):
            idx = self.reaction_index[reaction]
        metabolite_indeces = np.nonzero(self.stoichiometry[:, idx])[0]
        stoich_coef = self.stoichiometry[metabolite_indeces, idx]
        if by_name:
            return list(
                zip((self.metabolites[i] for i in metabolite_indeces), stoich_coef)
            )
        else:
            return list(zip(metabolite_indeces, stoich_coef))

    def participating_reactions(self, metabolite, by_name=True):
        """Retrieve all reactions in which this metabolites participates and the stoichiometry.

        :param metabolite: The metabolite of which you want to retrieve the reactions.
        :type metabolite: str
        :param by_name: Whether to return reactions by name or index.
        :type by_name: bool

        :returns: A list of reaction, stoichiometry tuples.
        :rtype: list[(reaction, n)]
        """
        if not isinstance(metabolite, int):
            idx = self.metabolite_index[metabolite]
        reaction_indeces = np.nonzero(self.stoichiometry[idx, :])[0]
        stoich_coef = self.stoichiometry[idx, reaction_indeces]
        if by_name:
            return list(zip((self.reactions[i] for i in reaction_indeces), stoich_coef))
        else:
            return list(zip(reaction_indeces, stoich_coef))

    def remove_reaction(self, reaction):
        """Remove a reaction and automatically update dependent other properties."""
        if not isinstance(reaction, int):
            idx = self.reaction_index[reaction]
        else:
            idx = reaction
            reaction = self.reactions[idx]

        # Delete the reaction, update stoichiometry matrix and repair the index.
        del self.reactions[idx]
        self.stoichiometry = np.delete(self.stoichiometry, idx, axis=1)
        self._reindex()

        # Remove flux and parameters only in that flux.
        del self.fluxes[reaction]
        self.remove_unused_parameters()

    def remove_metabolite(self, metabolite, remove_invalidated_reactions=True):
        """Remove a metabolite and automatically update dependent other properties."""
        if not isinstance(metabolite, int):
            idx = self.metabolite_index[metabolite]
        else:
            idx = metabolite
            metabolite = self.metabolites[idx]

        # If wanted, we remove the invalid reactions
        reactions = [name for name, s in self.participating_reactions(metabolite)]
        if remove_invalidated_reactions:
            for reaction in reactions:
                self.remove_reaction(reaction)
        else:
            warnings.warn("Created invalid reactions: {}".format(reactions))

        # Delete the metabolite, update stoichiometry matrix and repair the index.
        del self.metabolites[idx]
        self.stoichiometry = np.delete(self.stoichiometry, idx, axis=0)
        self._reindex()

        # Remove initial concentrations
        self.remove_unused_parameters()

    def remove_unused_parameters(self):
        """Remove any unused parameters from the model."""
        used_p_flux = set()
        if self.fluxes:
            symbols = [
                i.free_symbols
                for i in self.fluxes.values()
                if hasattr(i, "free_symbols")
            ]
            if symbols:
                used_p_flux = {i.name for i in set.union(*symbols)}

        used_p_obs = set()
        if self.observables:
            symbols = [
                i.free_symbols
                for i in self.observable_terms.values()
                if hasattr(i, "free_symbols")
            ]
            if symbols:
                used_p_obs = {i.name for i in set.union(*symbols)}
        used_parameters = list((used_p_flux | used_p_obs) - set(self.metabolites))

        self.parameters = [i for i in self.parameters if i in used_parameters]
        self.parameter_values = collections.OrderedDict(
            [(k, v) for k, v in self.parameter_values.items() if k in self.parameters]
        )

        self.initial_concentrations = collections.OrderedDict(
            [
                (k, v)
                for k, v in self.initial_concentrations.items()
                if k in self.metabolites
            ]
        )

    def save(self, filepath, version=3, level=2):
        """Write the SBML model to an SBML file.

        :param filepath: Location to save the SBML file.
        :param filepath: str or pathlib.Path
        """
        # Create new SBML document and model
        document = libsbml.SBMLDocument(version, level)
        model = document.createModel()

        # Create a main compartment
        c = model.createCompartment()
        c.setId("main")
        c.setName("main")
        c.setConstant(True)
        c.setSize(1)

        # If we have observables assign them all to a single compartment.
        if self.observables:
            oc = model.createCompartment()
            oc.setId("observables")
            oc.setName("observables")
            oc.setConstant(True)
            oc.setSize(1)

        # Add Metabolites
        for m_name in self.metabolites:
            m = model.createSpecies()
            m.setId(m_name)
            m.setName(m_name)
            try:
                m.setInitialConcentration(float(self.initial_concentrations[m_name]))
            except KeyError:
                pass
            m.setConstant(False)
            m.setCompartment(c.getId())
            m.setHasOnlySubstanceUnits(True)
            m.setBoundaryCondition(False)

        # Add Parameters
        for p_name in self.parameters:
            p = model.createParameter()
            p.setId(p_name)
            p.setName(p_name)
            try:
                p.setValue(float(self.parameter_values[p_name]))
            except KeyError:
                pass
            p.setConstant(True)

        # Add Reactions
        for i, r_name in enumerate(self.reactions):
            r = model.createReaction()
            r.setId(r_name)
            r.setName(r_name)
            if not (version == 3 and level >= 2):
                r.setFast(False)
            r.setReversible(True)

            row = self.stoichiometry[:, i]
            index = np.where(row != 0)[0]
            counts = [i for i in row[index]]
            metabolite_names = [self.metabolites[i] for i in index]

            # Add metabolites
            for s, m_name in zip(counts, metabolite_names):
                if s < 0:
                    m = r.createReactant()
                    s = abs(s)
                elif s > 0:
                    m = r.createProduct()
                else:
                    raise ValueError(
                        "Invalid stoichiometry for {} ({})".format(s, m_name)
                    )
                m.setStoichiometry(s)
                m.setSpecies(m_name)
                m.setConstant(True)

            # Check for any metabolites that are not reactants or products
            # and add those as modifiers.
            for symbol in self.fluxes[r_name].free_symbols:
                name = symbol.name
                if name in self.metabolites and name not in metabolite_names:
                    m = r.createModifier()
                    m.setSpecies(name)

            # Convert Sympy to ASTNode and attached to kinetic law.
            law = r.createKineticLaw()
            try:
                ASTNode = ASTNodeWrapper.from_sympy(self.fluxes[r_name]).node
            except KeyError:
                r.unsetKineticLaw()
            else:
                law.setMath(ASTNode)

        # Add observables
        for o_name in self.observables:
            o = model.createSpecies()
            o.setId(o_name)
            o.setName(o_name)
            o.setConstant(False)
            o.setCompartment(oc.getId())
            o.setHasOnlySubstanceUnits(True)
            o.setBoundaryCondition(False)
            o.setSBOTerm("SBO:0000406")

        # Add observable terms as Assignment rules
        for i, o_name in enumerate(self.observables):
            r = model.createAssignmentRule()
            r.setVariable(o_name)
            r.setMath(ASTNodeWrapper.from_sympy(self.observable_terms[o_name]).node)

        # Verify document
        document.checkConsistency()
        n_errors = document.getNumErrors()
        for i in range(n_errors):
            error = document.getError(i)
            if error.isWarning():
                continue
            else:
                raise RuntimeError(
                    "Constructed invalid SBML. First Error (line {}): {}".format(
                        error.getLine(), error.getShortMessage()
                    )
                )

        success = libsbml.writeSBMLToFile(document, str(filepath))
        if not success:
            raise ValueError("Could not write SBMLfile to: {}".format(filepath))


# Note: The following three functions are used to patch the Sympy MathMLPrinter in-place.
# This might not work on newer versions of Sympy but is essential to get SBML compatible
# MathML output. Tested with Sympy 1.1.1 and 1.2.
def _print_Symbol(self, sym):
    """Modified version of `sympy.printing.mathml.MathMLPrinter._print_Symbol`.

    This removes the replacement of greek characters, and the conversion of
    _ and ^ to sub and superscript, which will allow it to do a round trip
    through sympy <-> libsbml.
    """
    ci = self.dom.createElement(self.mathml_tag(sym))
    ci.appendChild(self.dom.createTextNode(sym.name))
    return ci


def _print_float(self, e):
    """For some reason the printer only has int defined and not float so we add it ourselves."""
    x = self.dom.createElement(self.mathml_tag(e))
    x.appendChild(self.dom.createTextNode(str(e)))
    return x


def _print_Pow(self, e):
    """Fixed version of _print_Pow.

    In the root case, this always uses an identifier (ci) as degree, while
    it might be a number (cn), leading to invalid SBML as it tries to look
    for a parameter with the name of a number.
    """
    # Here we use root instead of power if the exponent is the reciprocal of an integer
    if e.exp.is_Rational and e.exp.p == 1:
        x = self.dom.createElement("apply")
        x.appendChild(self.dom.createElement("root"))
        if e.exp.q != 2:
            xmldeg = self.dom.createElement("degree")
            # xmlci = self.dom.createElement('ci')
            # xmlci.appendChild(self.dom.createTextNode(str(e.exp.q)))
            xmldeg.appendChild(self._print(e.exp.q))
            x.appendChild(xmldeg)
        x.appendChild(self._print(e.base))
        return x

    x = self.dom.createElement("apply")
    x_1 = self.dom.createElement(self.mathml_tag(e))
    x.appendChild(x_1)
    x.appendChild(self._print(e.base))
    x.appendChild(self._print(e.exp))
    return x


def mathml_tag(self, e):
    """Modified version of `sympy.printing.mathml.MathMLPrinter.mathml_tag`.

    Adds the mapping of float -> mathml: cn

    Returns the MathML tag for an expression.
    """
    translate = {
        "Add": "plus",
        "Mul": "times",
        "Derivative": "diff",
        "Number": "cn",
        "int": "cn",
        "float": "cn",
        "Pow": "power",
        "Symbol": "ci",
        "Integral": "int",
        "Sum": "sum",
        "sin": "sin",
        "cos": "cos",
        "tan": "tan",
        "cot": "cot",
        "asin": "arcsin",
        "asinh": "arcsinh",
        "acos": "arccos",
        "acosh": "arccosh",
        "atan": "arctan",
        "atanh": "arctanh",
        "acot": "arccot",
        "atan2": "arctan",
        "log": "ln",
        "Equality": "eq",
        "Unequality": "neq",
        "GreaterThan": "geq",
        "LessThan": "leq",
        "StrictGreaterThan": "gt",
        "StrictLessThan": "lt",
    }

    for cls in e.__class__.__mro__:
        n = cls.__name__
        if n in translate:
            return translate[n]
    # Not found in the MRO set
    n = e.__class__.__name__
    return n.lower()


class ASTNodeWrapper(object):
    """Class for wrapping ASTNode of libsbml and converting it to Sympy and back."""

    functions = {
        libsbml.AST_FUNCTION,
        libsbml.AST_LAMBDA,
    }

    variables = {
        libsbml.AST_NAME,
    }

    numerical = {
        libsbml.AST_INTEGER,
        libsbml.AST_RATIONAL,
        libsbml.AST_REAL,
        libsbml.AST_REAL_E,
    }

    constants = {
        libsbml.AST_CONSTANT_E: sympy.E,
        libsbml.AST_CONSTANT_FALSE: sympy.false,
        libsbml.AST_CONSTANT_PI: sympy.pi,
        libsbml.AST_CONSTANT_TRUE: sympy.true,
        libsbml.AST_NAME_AVOGADRO: sympy.sympify("6.02214086e23"),
        # Not a true constant but can be treated as such for the conversion.
        libsbml.AST_NAME_TIME: sympy.Symbol("t"),
    }

    operators = {
        libsbml.AST_PLUS: operator.add,
        libsbml.AST_MINUS: operator.sub,
        libsbml.AST_POWER: operator.pow,
        libsbml.AST_FUNCTION_POWER: operator.pow,
        # div does not exist in python 3, so we use truediv.
        libsbml.AST_DIVIDE: operator.div if PYTHON_2 else operator.truediv,
        libsbml.AST_TIMES: operator.mul,
    }

    unary_operators = {
        libsbml.AST_PLUS: operator.pos,
        libsbml.AST_MINUS: operator.neg,
    }

    mathematical_functions = {
        libsbml.AST_FUNCTION_ABS: sympy.Abs,
        libsbml.AST_FUNCTION_ARCCOS: sympy.acos,
        libsbml.AST_FUNCTION_ARCCOSH: sympy.acosh,
        libsbml.AST_FUNCTION_ARCCOT: sympy.acot,
        libsbml.AST_FUNCTION_ARCCOTH: sympy.acoth,
        libsbml.AST_FUNCTION_ARCCSC: sympy.acsc,
        libsbml.AST_FUNCTION_ARCCSCH: sympy.acsch,
        libsbml.AST_FUNCTION_ARCSEC: sympy.asec,
        libsbml.AST_FUNCTION_ARCSECH: sympy.asech,
        libsbml.AST_FUNCTION_ARCSIN: sympy.asin,
        libsbml.AST_FUNCTION_ARCSINH: sympy.asinh,
        libsbml.AST_FUNCTION_ARCTAN: sympy.atan,
        libsbml.AST_FUNCTION_ARCTANH: sympy.atanh,
        libsbml.AST_FUNCTION_CEILING: sympy.ceiling,
        libsbml.AST_FUNCTION_COS: sympy.cos,
        libsbml.AST_FUNCTION_COSH: sympy.cosh,
        libsbml.AST_FUNCTION_COT: sympy.cot,
        libsbml.AST_FUNCTION_COTH: sympy.coth,
        libsbml.AST_FUNCTION_CSC: sympy.csc,
        libsbml.AST_FUNCTION_CSCH: sympy.csch,
        libsbml.AST_FUNCTION_EXP: sympy.exp,
        libsbml.AST_FUNCTION_FACTORIAL: sympy.factorial,
        libsbml.AST_FUNCTION_FLOOR: sympy.floor,
        libsbml.AST_FUNCTION_LN: sympy.ln,
        # This can be either log(base, expr) or log(expr) which should default to ln
        libsbml.AST_FUNCTION_LOG: lambda base, expr=None: (
            sympy.log(expr, base)
            # Note that base is the expression here!
            if expr is None
            else sympy.ln(base)
        ),
        libsbml.AST_FUNCTION_MAX: sympy.Max,
        libsbml.AST_FUNCTION_MIN: sympy.Min,
        # TODO: Are negatives correctly handled for quotient and rem?
        libsbml.AST_FUNCTION_QUOTIENT: operator.floordiv,
        libsbml.AST_FUNCTION_REM: operator.mod,
        # Note that root is defined as (root, expr) instead of a (expr, root) like pow.
        # So it should be mapped with the arguments in reverse.
        libsbml.AST_FUNCTION_ROOT: lambda root, expr: sympy.root(expr, root),
        libsbml.AST_FUNCTION_SEC: sympy.sec,
        libsbml.AST_FUNCTION_SECH: sympy.sech,
        libsbml.AST_FUNCTION_SIN: sympy.sin,
        libsbml.AST_FUNCTION_SINH: sympy.sinh,
        libsbml.AST_FUNCTION_TAN: sympy.tan,
        libsbml.AST_FUNCTION_TANH: sympy.tanh,
    }

    logical_functions = {
        libsbml.AST_LOGICAL_AND: sympy.And,
        libsbml.AST_LOGICAL_NOT: sympy.Not,
        libsbml.AST_LOGICAL_OR: sympy.Or,
        libsbml.AST_LOGICAL_XOR: sympy.Xor,
        libsbml.AST_LOGICAL_IMPLIES: sympy.Implies,
    }

    relational_functions = {
        libsbml.AST_RELATIONAL_EQ: sympy.Eq,
        libsbml.AST_RELATIONAL_GEQ: sympy.Ge,
        libsbml.AST_RELATIONAL_GT: sympy.Gt,
        libsbml.AST_RELATIONAL_LEQ: sympy.Le,
        libsbml.AST_RELATIONAL_LT: sympy.Lt,
        libsbml.AST_RELATIONAL_NEQ: sympy.Ne,
    }

    all_functions = {
        k: v
        for d in (
            operators,
            mathematical_functions,
            logical_functions,
            relational_functions,
        )
        for k, v in d.items()
    }

    not_implemented = {
        libsbml.AST_FUNCTION_DELAY: "AST_FUNCTION_DELAY",
        libsbml.AST_UNKNOWN: "AST_UNKNOWN",
        libsbml.AST_FUNCTION_PIECEWISE: "AST_FUNCTION_PIECEWISE",
        # Could be implemented with sympy.Piecewise, but need to check argument mapping.
        libsbml.AST_CSYMBOL_FUNCTION: "AST_CSYMBOL_FUNCTION",
        libsbml.AST_FUNCTION_RATE_OF: "AST_FUNCTION_RATE_OF",
        # Is this the derivate w.r.t time?
        # lambda x: sympy.diff(x, sympy.Symbol('t'))
    }

    # Modify MathML printing for LibSBML compatibility.
    from sympy.printing.mathml import MathMLPrinter

    # Override the Sympy definitions.
    MathMLPrinter._print_Symbol = _print_Symbol
    MathMLPrinter._print_Pow = _print_Pow
    MathMLPrinter._print_float = _print_float
    MathMLPrinter.mathml_tag = mathml_tag

    def __init__(self, ASTNode):
        """Create an ASTNode wrapper from a libsbml ASTNode.

        This wraps an ASTNode and can be used to generate a Sympy expression.

        :param ASTNode: ASTNode (A libsbml math node object, retrieved from a kinetic law.)
        :type ASTNode: libsbml.ASTNode

        Can also be created from a Sympy expression using ASTNodeWrapper.from_sympy.
        """
        self.node = ASTNode

    @classmethod
    def from_sympy(cls, sympy_expression):
        """Convert a sympy expression back to an ASTNode through MathML.

        Note: libsbml requires ascii input. A conversion using xmlcharrefreplace will be attempted,
        but might still produce invalid sbml.

        :param sympy_expression: A Sympy expression to be converted to a ASTNode for libsbml.
        :type sympy_expression: Sympy expression

        :raises ValueError: A ValueError will be read if the MathML produced by the Sympy
        expression is not valid SBML.
        """
        # Create MathML
        mathml = cls.MathMLPrinter().doprint(sympy_expression)
        # Add open and close tags and convert to ASCII for libsbml.
        math_open = "<math xmlns='http://www.w3.org/1998/Math/MathML'>"
        math_close = "</math>"

        mathml = str(
            "".join((math_open, mathml, math_close)).encode(
                "ascii", "xmlcharrefreplace"
            ),
            "ascii",
        )

        # Check validity.
        ASTNode = libsbml.readMathMLFromString(mathml)
        if ASTNode is None or not ASTNode.isWellFormedASTNode:
            raise ValueError(
                "Invalid MathML input to readMathMLFromString: \n{}".format(mathml)
            )

        return cls(ASTNode)

    def __iter__(self):
        """Iterate through the direct children nodes."""
        for i in range(self.node.getNumChildren()):
            yield ASTNodeWrapper(self.node.getChild(i))

    @property
    def type(self):
        """Node type."""
        return self.node.getType()

    @property
    def value(self):
        """Numerical value."""
        if self.type == libsbml.AST_INTEGER:
            return self.node.getInteger()
        if self.type == libsbml.AST_RATIONAL:
            return self.node.getNumerator() / self.node.getDenominator()
        elif self.type in self.numerical:
            return self.node.getReal()
        else:
            raise ValueError("Not a numerical value.")

    @property
    def name(self):
        """Variable of function name."""
        try:
            return self.node.getName()
        except AttributeError:
            raise ValueError("Not a named value")

    def to_sympy(self):
        """Recursively convert the AST to a Sympy representation.

        :returns: A Sympy expression of the ASTNode.
        :rtype: Sympy expression

        :raises NotImplementedError: A NotImplementedError is raised if an unsupported
        type is encountered. See ASTNodeWrapper.not_implemented for the list of unsupported types.

        Note: This *will* crash for highly nested sbml math due to exceeding the recursion depth.
        Either rewrite this using an explicit stack or increase the allowed recursion depth
        if this happens (see 'sys.setrecursionlimit').
        """
        if self.type in self.not_implemented:
            raise NotImplementedError(
                "{} not implemented.".format(self.not_implemented[self.type])
            )
        elif self.type in self.constants:
            return self.constants[self.type]
        elif self.type in self.variables:
            return sympy.Symbol(self.name)
        elif self.type in self.numerical:
            return sympy.sympify(self.value)
        elif self.type in self.all_functions or self.type in self.functions:
            children = [child.to_sympy() for child in self]
            if len(children) == 1 and self.type in self.operators:
                return self.unary_operators[self.type](*children)
            elif self.type in self.all_functions:
                return self.all_functions[self.type](*children)
            else:
                if self.type == libsbml.AST_LAMBDA:
                    return sympy.Lambda(children[:-1], children[-1])
                elif self.type == libsbml.AST_FUNCTION:
                    return sympy.Function(self.name)(*children)
                else:
                    raise ValueError("Unsupported ASTNode type: {}".format(self.type))
        raise ValueError("Unrecognised ASTNode: {}".format(self))

    def __str__(self):
        return "ASTNodeWrapper({})".format(libsbml.formulaToString(self.node))

    def __repr__(self):
        return str(self)
