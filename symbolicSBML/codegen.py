"""Code generation for transforming a SymbolicModel into simulation code.

Starting from a SymbolicModel the folliwing steps are performed:
    1) Generate code in the appriopriate language.
    2) Add set-up code etc.
    3) Write the code to files.
    4) [Optional] Compile the code.
    5) Create an interface to call the generated code from python.

The interface can span multiple levels of usability:
    A) Only basic function calls
        - dydt(t, p, state)
        - jac(t, p, state)
        - flux(t, p, state)
        - dydp(t, p, state)
        - dfdp(t, p, state)
    B) Complete simulation of ODE:
        - run(t, p, state)
    C) Extended uses (Not implemented):
        - Optimization
        - Sensitivity (whole trajactory)
            - forward sensitivity
            - adjoint sensitivity
    D) Events

Implemented:
    - JitCode (A/B) - Implemented as to_jitcode_ODE method of symbolic_model.SymbolicModel.
    - Sympy (A) - SympyODE
    - Python (A) - SympyLambdifyODE
    - C (A) - CythonODEBuilder Through Cython (slightly different interface)
Note that these implementations are for testing purposes, more mature & feature-rich libraries
can be used by conversion to SBML format.

Possible implementations:
    1) Matlab / Octave (through MatlabEngine)
    2) Julia (through PyJulia)
    3) C / C++ (using jitcode, cython or scipy.weave)
    4) Fortan (using F2PY)
    5) Pure python (plain, numpy, numexpr or numba)

Author: Rik van Rosmalen
"""
import sys
import subprocess
import importlib
import pathlib

import sympy
import numpy as np


class BaseODE:
    """Base interface for ODE object."""

    def __init__(self, symbolicModel):
        self._model = symbolicModel

    @property
    def model(self):
        return self._model

    def dydt(self, t, p, state):
        raise NotImplementedError

    def jacobian(self, t, p, state):
        raise NotImplementedError

    def flux(self, t, p, state):
        raise NotImplementedError

    def sensitivity(self, t, p, state):
        raise NotImplementedError

    def flux_sensitivity(self, t, p, state):
        raise NotImplementedError

    def simulate(self, t, p, state):
        raise NotImplementedError


class SympyODE(BaseODE):
    """Symbolic version of the ODE system.

    Can be passed a mixture of symbolic or numeric values. Will return Sympy Matrices.
    """

    def __init__(self, symbolicModel):
        super().__init__(symbolicModel)
        for eq_name, model_name in [
            ("dydt", "dydt"),
            ("jacobian", "jacobian"),
            ("flux", "fluxes"),
            ("sensitivity", "sensitivity"),
            ("flux_sensitivity", "flux_sensitivity"),
        ]:

            def wrapper(self, t, state, p, model_name=model_name):
                # Set model_name as a default argument to prevent late binding of closure.
                variables = self._map_to_symbols(t, p, state)
                return getattr(self.model, model_name).xreplace(variables).T

            wrapper.__doc__ = """Calculate {}.

                :param t: Time
                :type t: sympy expression
                :param state: State vector (metabolite concentrations)
                :type state: [sympy expression]
                :param p: Parameter vector
                :type p: [sympy expression]
                :param model_name: Which expression to calculate, ignore.
                """.format(
                eq_name
            )
            wrapper.__name__ = eq_name
            setattr(self, eq_name, wrapper.__get__(self))

    def _map_to_symbols(self, t, p, state):
        variables = {self.model.t_symbol: t}
        variables.update(dict(zip(self.model.parameter_symbols, p)))
        variables.update(dict(zip(self.model.metabolite_symbols, state.T)))
        return variables


class SympyLambdifyODE(BaseODE):
    """Python version of the ODE system.

    Faster than SympyODE, but only accepts numeric values.
    """

    def __init__(self, symbolicModel, lambdify_args=None):
        super().__init__(symbolicModel)
        if lambdify_args is None:
            lambdify_args = {"modules": ["numpy"]}
        t, p, y = self.model.t_symbol, self.model._par_ms, self.model._met_ms
        for eq_name, model_name in [
            ("dydt", "dydt"),
            ("jacobian", "jacobian"),
            ("flux", "fluxes"),
            ("sensitivity", "sensitivity"),
            ("flux_sensitivity", "flux_sensitivity"),
        ]:
            eq = self.model._sym_matrix_replace(getattr(self.model, model_name))
            f = sympy.lambdify([t, y, p], eq, **lambdify_args)

            def wrapper(self, t, state, p, f=f):
                # Set f as a default argument to prevent late binding of closure.
                return f(t, np.atleast_2d(state).T, np.atleast_2d(p).T).T

            wrapper.__doc__ = """Calculate {}.

                :param t: Time
                :type t: float
                :param state: State vector (metabolite concentrations)
                :type state: [float] or np.array[float]
                :param p: Parameter vector
                :type p: [float] or np.array[float]
                :param f: Which expression to calculate, ignore.
                """.format(
                eq_name
            )
            wrapper.__name__ = eq_name
            setattr(self, eq_name, wrapper.__get__(self))


CYTHON_BUILD_TEMPLATE = '''"""Automatically generated Cython module build template.
Do not modify: changes might be overwritten.

Author: symbolicSBML.CythonODE (Rik van Rosmalen / rik.vanrosmalen@wur.nl)
"""
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy

extensions = [Extension('{module_name}',
                        sources=['{pyx_name}.pyx', '{source_file}'],
                        extra_compile_args=[{compile_args}],
                        )
              ]
extensions[0].include_dirs += [numpy.get_include()]
setup(
    name='{module_name}',
    ext_modules=cythonize(extensions, nthreads={build_threads}),
    cmdclass=dict([('build_ext', build_ext)]),
)
'''

CYTHON_TEMPLATE = '''"""Automatically generated Cython module build template.
Do not modify: changes might be overwritten.

Author: symbolicSBML.CythonODE (Rik van Rosmalen / rik.vanrosmalen@wur.nl)
"""
# cython: language_level=3, embedsignature=True
cimport cython

import numpy as np
cimport numpy as cnp

cdef extern from "{header_file}":
    void {flux_function}(double *{p}, double *{y}, double *{flux})
    void {ode_function}(double *{p}, double *{y}, double *{dydt})
{skip_jacobian}    void {jac_function}(double *{p}, double *{y}, double *{J})
{skip_sensitivity}    void {sensitivity_function}(double *{p}, double *{y}, double *{dydp})
{skip_sensitivity}    void {flux_sensitivity_function}(double *{p}, double *{y}, double *{dfdp})

cdef class CythonODE(object):
    """A optimized Cython ODE system.

    Significantly more performant then SympyLambdifyODE, but requires compilation.
    Note that the interface is slightly different as parameters should be set manually.
    """
    # We re-use the same views for the c bindings to avoid overhead.
    # Note that all numpy arrays received as input are copied and converted to C-layout.
    # All outputs are also copied. This is done to avoid bugs and subtle interations
    # with numpy's system of views and actual copies.
    cdef double[:] p_view
    cdef double[:] flux_view
    cdef double[:] dy_view
{skip_jacobian}    cdef double[:,:] J_view
{skip_sensitivity}    cdef double[:,:] dydp_view
{skip_sensitivity}    cdef double[:,:] dfdp_view

    ODE_DTYPE = np.double

    n_parameters = {size_p}
    n_metabolites = {size_m}
    n_reactions = {size_r}

    parameters = np.array(["{parameters}"])
    metabolites = np.array(["{metabolites}"])
    reactions = np.array(["{reactions}"])

    @cython.boundscheck({boundscheck})
    @cython.wraparound({wraparound})
    def __init__(self):
        """Generated ODE system."""
        self.p_view = np.zeros(({size_p},), dtype=np.double, order='C')
        self.dy_view = np.zeros(({size_m},), dtype=np.double, order='C')
        self.flux_view = np.zeros(({size_r},), dtype=np.double, order='C')
{skip_jacobian}        self.J_view = np.zeros(({size_m}, {size_m}), dtype=np.double, order='C')
{skip_sensitivity}        self.dydp_view = np.zeros(({size_m}, {size_p}), dtype=np.double, order='C')
{skip_sensitivity}        self.dfdp_view = np.zeros(({size_r}, {size_p}), dtype=np.double, order='C')

    @cython.boundscheck({boundscheck})
    @cython.wraparound({wraparound})
    def get_current_parameters(self):
        """View the current parameters of the ODE system."""
        return np.asarray(self.p_view){copy_results}

    @cython.boundscheck({boundscheck})
    @cython.wraparound({wraparound})
    def update_parameters(self, cnp.ndarray[cnp.double_t, ndim=1] p):
        """Update the parameters of the ODE system.

        Input:
        p  - parameters: numpy array [double {size_p}x1]
        """
        cdef int px, py
        px = p.shape[0]
        py = p.ndim
        if px != {size_p} or py != 1:
            raise ValueError("Wrong dimension for p, should be [{size_p}x1]")
        self.p_view = np.require(p.copy(order='C'), dtype=np.double, requirements='C')

    @cython.boundscheck({boundscheck})
    @cython.wraparound({wraparound})
    def dydt(self, double t, double[:] y):
        """Calculate state change (integration step) for the state y.

        Input:
        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
        y  - state: numpy array [double {size_m}x1]
        Output:
        dy - state change: numpy array [double {size_m}x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != {size_m}:
            raise ValueError("Wrong dimension for y, should be [{size_m}x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        {ode_function}(&self.p_view[0], &y[0], &self.dy_view[0])
        return np.asarray(self.dy_view){copy_results}

    @cython.boundscheck({boundscheck})
    @cython.wraparound({wraparound})
    def flux(self, double[:] y):
        """Calculate reaction flux for the state y.

        Input:
        y  - state: numpy array [double {size_m}x1]
        Output:
        flux - state change: numpy array [double {size_r}x1]
        """
        cdef int yx
        yx = y.shape[0]
        if yx != {size_m}:
            raise ValueError("Wrong dimension for y, should be [{size_m}x1]")

        y = np.require(y, dtype=np.double, requirements='C')
        {flux_function}(&self.p_view[0], &y[0], &self.flux_view[0])
        return np.asarray(self.flux_view){copy_results}

{skip_jacobian}    @cython.boundscheck({boundscheck})
{skip_jacobian}    @cython.wraparound({wraparound})
{skip_jacobian}    def jacobian(self, double t, double[:] y):
{skip_jacobian}        """Calculate the Jacobian for the state y (dy/dx).
{skip_jacobian}
{skip_jacobian}        Input:
{skip_jacobian}        t  - time: [double] -> Ignored. (For scipy.integrate.ode compatibility)
{skip_jacobian}        y  - state: numpy array [double {size_m}x1]
{skip_jacobian}        Output:
{skip_jacobian}        J  - Jacobian: numpy array [double {size_m}x{size_m}]
{skip_jacobian}        """
{skip_jacobian}        cdef int yx
{skip_jacobian}        yx = y.shape[0]
{skip_jacobian}        if yx != {size_m}:
{skip_jacobian}            raise ValueError("Wrong dimension for y, should be [{size_m}x1]")
{skip_jacobian}
{skip_jacobian}        y = np.require(y, dtype=np.double, requirements='C')
{skip_jacobian}        {jac_function}(&self.p_view[0], &y[0], &self.J_view[0, 0])
{skip_jacobian}        return np.asarray(self.J_view){copy_results}


{skip_sensitivity}    @cython.boundscheck({boundscheck})
{skip_sensitivity}    @cython.wraparound({wraparound})
{skip_sensitivity}    def dydp(self, double[:] y):
{skip_sensitivity}        """Calculate the parametric sensitivity for the state y (dy/dp).
{skip_sensitivity}
{skip_sensitivity}        Input:
{skip_sensitivity}        y  - state: numpy array [double {size_m}x1]
{skip_sensitivity}        Output:
{skip_sensitivity}        dydp  - parametric sensitivity: numpy array [double {size_m}x{size_p}]
{skip_sensitivity}        """
{skip_sensitivity}        cdef int yx
{skip_sensitivity}        yx = y.shape[0]
{skip_sensitivity}        if yx != {size_m}:
{skip_sensitivity}            raise ValueError("Wrong dimension for y, should be [{size_m}x1]")
{skip_sensitivity}
{skip_sensitivity}        y = np.require(y, dtype=np.double, requirements='C')
{skip_sensitivity}        {sensitivity_function}(&self.p_view[0], &y[0], &self.dydp_view[0, 0])
{skip_sensitivity}        return np.asarray(self.dydp_view){copy_results}

{skip_sensitivity}    @cython.boundscheck({boundscheck})
{skip_sensitivity}    @cython.wraparound({wraparound})
{skip_sensitivity}    def dfdp(self, double[:] y):
{skip_sensitivity}        """Calculate the parametric flux sensitivity for the fluxes (df/dp).
{skip_sensitivity}
{skip_sensitivity}        Input:
{skip_sensitivity}        y  - state: numpy array [double {size_m}x1]
{skip_sensitivity}        Output:
{skip_sensitivity}        dydp  - parametric flux sensitivity: numpy array [double {size_r}x{size_p}]
{skip_sensitivity}        """
{skip_sensitivity}        cdef int yx
{skip_sensitivity}        yx = y.shape[0]
{skip_sensitivity}        if yx != {size_m}:
{skip_sensitivity}            raise ValueError("Wrong dimension for y, should be [{size_m}x1]")
{skip_sensitivity}
{skip_sensitivity}        y = np.require(y, dtype=np.double, requirements='C')
{skip_sensitivity}        {flux_sensitivity_function}(&self.p_view[0], &y[0], &self.dfdp_view[0, 0])
{skip_sensitivity}        return np.asarray(self.dfdp_view){copy_results}
'''

# HEAVISIDE_HEADER = '''double Heaviside(double x);
# '''

# HEAVISIDE_IMPL = '''
# double Heaviside(double x) {
#     if (x <= 0.0) {
#         return 0.0;
#     }
#     return 1.0;
# }
# '''


class CythonODEBuilder:
    """Class to construct and build a cython ODE object."""

    def __init__(self, symbolicModel, **kwargs):
        self.model = symbolicModel
        self.args = {
            "module_name": "ode",
            "pyx_name": "ode",
            "ode_function": "dydt",
            "jac_function": "jacobian",
            "flux_function": "flux",
            "sensitivity_function": "sensitivity",
            "flux_sensitivity_function": "flux_sensitivity",
            "y": self.model._met_ms,
            "p": self.model._par_ms,
            "flux": "flux",
            "dydt": "dydt",
            "J": "jacobian",
            "dydp": "dydp",
            "dfdp": "dfdp",
            "size_m": len(self.model.metabolites),
            "size_r": len(self.model.reactions),
            "size_p": len(self.model.parameters),
            "metabolites": '","'.join(self.model.metabolites),
            "reactions": '","'.join(self.model.reactions),
            "parameters": '","'.join(self.model.parameters),
            "boundscheck": False,
            "wraparound": False,
            "compile_args": ["-O3"],
            "build_threads": 4,
            "skip_jacobian": False,
            "skip_sensitivity": True,
            "copy_results": True,
        }
        self.args.update(kwargs)
        self.args["copy_results"] = ".copy()" if self.args["copy_results"] else ""
        self.args["skip_jacobian"] = "#" if self.args["skip_jacobian"] else ""
        self.args["skip_sensitivity"] = "#" if self.args["skip_sensitivity"] else ""
        self.args["compile_args"] = ", ".join(
            repr(i) for i in self.args["compile_args"]
        )
        self.files = None
        self.build_dir = None

    def build(self, directory):
        """Build the cython extension in the directory."""
        self._create_files()
        self._save_files(directory, self.files)
        self.build_dir = pathlib.Path(directory)

        try:
            python = sys.executable
            if not python:
                raise RuntimeError("Could not find python executable to run setup.py.")
            subprocess.check_output(
                [python, "setup.py", "build_ext", "--inplace"],
                cwd=str(self.build_dir),
                stderr=subprocess.STDOUT,
            )
        except subprocess.CalledProcessError as E:
            print(str(E.output))
            print(E.returncode)
            raise

    def _create_files(self):
        """Create the files required for code generation."""
        equations = ["dydt", "flux"]
        if self.args["skip_jacobian"] in (False, ""):
            equations.append("jacobian")
        if self.args["skip_sensitivity"] in (False, ""):
            equations.append("sensitivity")
            equations.append("flux_sensitivity")
        [(cf, cs), (hf, hs)] = self.model.generate_code(equations, "c")

        # # Insert Heaviside definition.
        # start = hs.find('#endif') - 1
        # hs = ''.join((hs[:start], HEAVISIDE_HEADER, hs[start:]))
        # start = cs.find('#include <math.h>') + len('#include <math.h>') + 1
        # cs = ''.join((cs[:start], HEAVISIDE_IMPL, cs[start:]))

        self.args["header_file"] = hf
        self.args["source_file"] = cf

        cython_build = CYTHON_BUILD_TEMPLATE.format(**self.args)
        cython_source = CYTHON_TEMPLATE.format(**self.args)

        # Define all files as (name, content)
        # Note that we create an empty __init__.py file so we can import everything as a module.
        self.files = [
            (cf, cs),
            (hf, hs),
            ("{}.pyx".format(self.args["pyx_name"]), cython_source),
            ("setup.py", cython_build),
            ("__init__.py", ""),
        ]

    def _save_files(self, directory, files):
        """Create an output directory and write the files to it.

        :param directory: directory to write output files.
        :type directory: str or path
        :param files: List of filename, filecontents pairs.
        :type files: list[tuple[str, str]]
        """
        base_path = directory
        pathlib.Path(base_path).mkdir(parents=True, exist_ok=True)

        for filename, content in files:
            filepath = base_path / pathlib.Path(filename)
            filepath.write_text(content)

    def create(self):
        """Create and return a Cython ODE instance."""
        sys.path.insert(0, str(self.build_dir))
        try:
            # Cache invalidation is smart since the module name
            # might be similar for different imported models.
            importlib.invalidate_caches()
            extension_module = importlib.import_module(self.args["module_name"])
            return extension_module.CythonODE()
        finally:
            if sys.path[0] == str(self.build_dir):
                sys.path.pop(0)
