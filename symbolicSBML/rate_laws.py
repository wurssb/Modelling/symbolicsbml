"""Functions to generate rate laws.

Currently allows for the following:
    - Modular rate laws (all types and parametrizations)
        - Regulation is currently not yet supported
    - Mass action (reversible and irreversible)

Also contains the corresponding SBO terms.

TODO:
    - Other rate laws
    - Modular rate law type per reaction
        - Rate law detection based on SBO term
    - Including regulation (as marked per SBO term)
    - Including enzyme cases:
        - Multiple enzymes per reaction
        - Multiple reactions per enzyme
        - Enzyme as SBML species instead of parameter
    - Including compartment volumes (when appriopriate)
        - Enzyme concentration vs count
    - Simplication of parameters:
        - kcat + enzyme concetration -> Vmax
        - kv + enzyme concentration -> Vmax*
        - Combined km for directing binding modular (DM) and power-law modular (PM) rate law
Author: Rik van Rosmalen
"""
import numpy as np
import sympy

from .parameters import Parameters


def modular_rate_law(
    reaction,
    metabolites,
    parametrisation="cat",
    sub_rate_law="CM",
    regulation=None,
    cooperativities=1,
    substitutions=None,
    set_assumptions=False,
):
    """Create a rate law term for a reaction.

    :param reaction: Reaction name
    :type reaction: str
    :param metabolites: Mapping of each metabolite to their stoichiometry.
    :type metabolites: dict[str -> int]
    :param parametrisation: Type of parametrisation: {cat, hal or weg}.
    :type parametrisation: str
    :param sub_rate_law: Type of denominator term: {CM, SM, DM, FM or PM}.
    :type sub_rate_law: str
    :param regulation: NotImplemented
    :type regulation: TBD
    :param cooperativities: The cooperativity factor, defaults to 1.
    :type cooperativities: number / symbol / str (will be converted to symbol)
    :param substitutions: Allowed subsitutions.
    :type substitutions: iterable of {'enzyme_rate', 'km_merged', 'km_rate'} or 'all'
    Note that km_rate implies km_merged and u_v / v_max.
    :param set_assumptions: Set assumptions on the sympy variables (real values for all
    metabolites concentrations and parameters, positive for a non-logarithmic parameters and
    metabolite concentrations.)
    :type set_assumptions: bool, (default=False)

    :returns: A sympy expression for the rate law.
    :rtype: sympy expression

    :raises ValueError: Raises a ValueError if an invalid parametrisation
    or sub_rate_law is passed.

    """
    if regulation is not None:
        raise NotImplementedError

    sub_rate_law = sub_rate_law.upper()
    parametrisation = parametrisation.lower()

    if substitutions == "all":
        allowed_substitutions = {"enzyme_rate", "km_merged", "km_rate"}
    elif substitutions is None:
        allowed_substitutions = set()
    else:
        allowed_substitutions = set(substitutions)

    required_parameters = {
        "cat": Parameters.cat | {"c_p"},
        "hal": Parameters.hal,
        "weg": Parameters.weg,
    }[parametrisation]

    if sub_rate_law != "PM":
        required_parameters.add("c_p")

    h = sympy.sympify(cooperativities)

    # Note that we need to take -s for reactants,
    # since we always assume the stoichiometry to be positive.
    reactants = [(m_id, -s) for m_id, s in metabolites.items() if s < 0]
    products = [(m_id, s) for m_id, s in metabolites.items() if s > 0]

    P = {"c": {}, "km": {}, "mu": {}, "c_p": {}}
    for parameter in required_parameters:
        if parameter == "c_p":
            # This is done later, after making sure the km and c is defined.
            continue
        elif parameter in Parameters.specific_to_metabolite:
            for metabolite, _ in reactants + products:
                # Special case for metabolites - we generally don't want the c__ in front.
                if parameter == Parameters.c:
                    symbol = sympy.Symbol(metabolite)
                else:
                    symbol = Parameters.as_symbol(
                        parameter, metabolite, reaction, set_assumptions
                    )
                P[parameter][metabolite] = symbol
        else:
            symbol = Parameters.as_symbol(parameter, None, reaction, set_assumptions)
            P[parameter] = symbol

    if "c_p" in required_parameters:
        for metabolite, _ in reactants + products:
            symbol = P["c"][metabolite] / P["km"][metabolite]
            P["c_p"][metabolite] = symbol

    # Prepare any substitutions
    # TODO: Case where we want to merge km + rate but keep u seperate.
    substitutions = {}
    if (
        "enzyme_rate" in allowed_substitutions
        or "km_merged" in allowed_substitutions
        or "km_rate" in allowed_substitutions
    ):
        # hal and weg use kV + u => u_v
        if parametrisation in {"hal", "weg"}:
            u_v = Parameters.as_symbol(Parameters.u_v, None, reaction, set_assumptions)
            substitutions[P[Parameters.kv] * P[Parameters.u]] = u_v
        # cat uses kcat+- + u => vmax+-
        elif parametrisation == "cat":
            vmax_prod = Parameters.as_symbol(
                Parameters.vmax_prod, None, reaction, set_assumptions
            )
            substitutions[P[Parameters.kcat_prod] * P[Parameters.u]] = vmax_prod
            vmax_sub = Parameters.as_symbol(
                Parameters.vmax_sub, None, reaction, set_assumptions
            )
            substitutions[P[Parameters.kcat_sub] * P[Parameters.u]] = vmax_sub

    if "km_merged" in allowed_substitutions or "km_rate" in allowed_substitutions:
        if sub_rate_law not in {"DM", "FM", "PM"}:
            raise ValueError(
                "Invalid substitution ({}) for sub rate law {}.".format(
                    "km_merged", sub_rate_law
                )
            )
        if "km_rate" in allowed_substitutions and sub_rate_law != "PM":
            raise ValueError(
                "Invalid substitution ({}) for sub rate law {}.".format(
                    "km_rate", sub_rate_law
                )
            )
        # Gather product and substrate terms into two groups
        # Don't forget to incorporate the cooperativity and stoichiometry!
        km_prod = Parameters.as_symbol(
            Parameters.km_prod, None, reaction, set_assumptions
        )
        km_prod_terms = sympy.Mul(*(P["km"][i] ** (s * h) for i, s in products))

        km_sub = Parameters.as_symbol(
            Parameters.km_sub, None, reaction, set_assumptions
        )
        km_sub_terms = sympy.Mul(*(P["km"][i] ** (s * h) for i, s in reactants))

        if (
            sub_rate_law == "PM"
            and parametrisation in {"weg", "hal"}
            and "km_rate" in allowed_substitutions
        ):
            # For PM + weg / hal we can merge the km + u_v
            km_tot_rate = Parameters.as_symbol(
                Parameters.km_tot_rate, None, reaction, set_assumptions
            )
            expr = (
                P[Parameters.kv]
                * P[Parameters.u]
                / sympy.sqrt(km_sub_terms * km_prod_terms)
            )
            substitutions[expr] = km_tot_rate
            # Remove the more simple substitution
            del substitutions[P[Parameters.kv] * P[Parameters.u]]

        elif sub_rate_law == "PM" and "km_rate" in allowed_substitutions:
            raise NotImplementedError("Buggy implementation: TODO")
            # For PM + kcat we can merge vmax+ + km_sub and vmax- + km_prod
            km_prod_rate = Parameters.as_symbol(
                Parameters.km_prod_rate, None, reaction, set_assumptions
            )
            expr = P[Parameters.kcat_prod] * P[Parameters.u] / km_prod_terms
            substitutions[expr] = km_prod_rate
            del substitutions[P[Parameters.kcat_prod] * P[Parameters.u]]

            km_sub_rate = Parameters.as_symbol(
                Parameters.km_sub_rate, None, reaction, set_assumptions
            )
            expr = P[Parameters.kcat_sub] * P[Parameters.u] / km_sub_terms
            substitutions[expr] = km_sub_rate
            del substitutions[P[Parameters.kcat_sub] * P[Parameters.u]]

        elif (
            sub_rate_law in {"PM", "FM"}
            and parametrisation in {"weg", "hal"}
            and "km_rate" not in allowed_substitutions
        ):
            if parametrisation == "hal" or sub_rate_law == "FM":
                raise NotImplementedError("Buggy implementation: TODO")
            # For FM / PM + weg / hal we can still merge all the kms
            km_tot = Parameters.as_symbol(
                Parameters.km_tot, None, reaction, set_assumptions
            )
            substitutions[km_sub_terms * km_prod_terms] = km_tot
            # This might be easier to substitute instead.
            substitutions[
                1 / sympy.sqrt(km_sub_terms * km_prod_terms)
            ] = 1 / sympy.sqrt(km_tot)

        elif "km_rate" not in allowed_substitutions:
            raise NotImplementedError("Buggy implementation: TODO")
            # Final case: just merge the kms for products and substrates seperately.
            # This holds for:
            #   DM + weg / hal / cat
            #   PM / FM + cat
            substitutions[km_prod_terms] = km_prod
            # substitutions[1 / sympy.sqrt(km_prod_terms)] = 1 / sympy.sqrt(km_prod)
            substitutions[km_sub_terms] = km_sub
            # substitutions[1 / sympy.sqrt(km_sub_terms)] = 1 / sympy.sqrt(km_sub)
        else:
            raise ValueError("Invalid combinatation of substitutions and rate law.")

    # Construct numerator
    if parametrisation == "cat":
        numerator = P[Parameters.kcat_sub] * sympy.Mul(
            *(P["c_p"][i] ** (s * h) for i, s in reactants)
        ) - P[Parameters.kcat_prod] * sympy.Mul(
            *(P["c_p"][i] ** (s * h) for i, s in products)
        )
    elif parametrisation == "hal":
        numerator = (
            P[Parameters.kv]
            * (
                sympy.sqrt(P[Parameters.keq] ** h)
                * sympy.Mul(*(P["c"][i] ** (s * h) for i, s in reactants))
                - sympy.sqrt(P[Parameters.keq] ** -h)
                * sympy.Mul(*(P["c"][i] ** (s * h) for i, s in products))
            )
            / sympy.sqrt(
                sympy.Mul(
                    *(P["km"][i] ** (sympy.Abs(s) * h) for i, s in metabolites.items())
                )
            )
        )
    elif parametrisation == "weg":
        numerator = (
            P[Parameters.kv]
            * (
                sympy.exp(
                    -(h * sympy.Add(*(P["mu"][i] * s for i, s in metabolites.items())))
                    / (2 * P[Parameters.R] * P[Parameters.T])
                )
                * sympy.Mul(*(P["c"][i] ** (s * h) for i, s in reactants))
                - sympy.exp(
                    (h * sympy.Add(*(P["mu"][i] * s for i, s in metabolites.items())))
                    / (2 * P[Parameters.R] * P[Parameters.T])
                )
                * sympy.Mul(*(P["c"][i] ** (s * h) for i, s in products))
            )
            / sympy.sqrt(
                sympy.Mul(
                    *(P["km"][i] ** (sympy.Abs(s) * h) for i, s in metabolites.items())
                )
            )
        )
    else:
        raise ValueError(
            "Invalid numerator parametrisation for modular "
            "rate law: {}. Must be one of {{cat, hal, weg}}".format(parametrisation)
        )

    # Construct denominator
    if sub_rate_law == "CM":
        denominator = (
            sympy.Mul(*((1 + P["c_p"][i]) ** (s * h) for i, s in reactants))
            + sympy.Mul(*((1 + P["c_p"][i]) ** (s * h) for i, s in products))
            - 1
        )
    elif sub_rate_law == "SM":
        denominator = sympy.Mul(
            *((1 + P["c_p"][i]) ** (sympy.Abs(s) * h) for i, s in metabolites.items())
        )
    elif sub_rate_law == "DM":
        denominator = (
            sympy.Mul(*(P["c_p"][i] ** (s * h) for i, s in reactants))
            + sympy.Mul(*(P["c_p"][i] ** (s * h) for i, s in products))
            + 1
        )
    elif sub_rate_law == "FM":
        denominator = sympy.Mul(
            *(P["c_p"][i] ** (sympy.Abs(s) * h / 2) for i, s in metabolites.items())
        )
    elif sub_rate_law == "PM":
        denominator = 1
    else:
        raise ValueError(
            "Invalid denominator rate law for modular "
            "rate law: {}. Must be on of {{CM, SM, DM, FM, PM}}".format(sub_rate_law)
        )

    # TODO: Regulation
    f_reg = 1
    d_reg = 0

    final_expression = P[Parameters.u] * f_reg * numerator / (denominator + d_reg)

    # Apply any substitutions
    final_expression = final_expression.subs(substitutions)
    # Simplication might be nice, but in can be quite slow, so leave it to the caller.

    return final_expression


def rev_mass_action_rate_law(reaction, metabolites):
    """Create a rate law term for a reaction.

    :param reaction: Reaction name
    :type reaction: str
    :param metabolites: Mapping of each metabolite to their stoichiometry.
    :type metabolites: dict[str -> int]

    :returns: A sympy expression for the rate law.
    :rtype: sympy expression
    """
    kfw = sympy.Symbol(Parameters.separator.join(("k_fw", reaction)))
    krv = sympy.Symbol(Parameters.separator.join(("k_rv", reaction)))

    # Note that we need to take -s for reactants,
    # since we always assume the stoichiometry to be positive.
    reactants = [(m_id, -s) for m_id, s in metabolites.items() if s < 0]
    products = [(m_id, s) for m_id, s in metabolites.items() if s > 0]

    con = {}
    for term_id in metabolites.keys():
        con[term_id] = sympy.Symbol(term_id)

    fw_rate = kfw * sympy.Mul(*(con[i] ** s for i, s in reactants))
    rv_rate = krv * sympy.Mul(*(con[i] ** s for i, s in products))

    return fw_rate - rv_rate


def irrev_mass_action_rate_law(reaction, metabolites):
    """Create a rate law term for a reaction.

    :param reaction: Reaction name
    :type reaction: str
    :param metabolites: Mapping of each metabolite to their stoichiometry.
    :type metabolites: dict[str -> int]

    :returns: A sympy expression for the rate law.
    :rtype: sympy expression
    """
    k = sympy.Symbol(Parameters.separator.join(("k", reaction)))

    # Note that we need to take -s for reactants,
    # since we always assume the stoichiometry to be positive.
    reactants = [(m_id, -s) for m_id, s in metabolites.items() if s < 0]

    con = {}
    for term_id in metabolites.keys():
        con[term_id] = sympy.Symbol(term_id)

    return k * sympy.Mul(*(con[i] ** s for i, s in reactants))


available_rate_laws = {
    "modular": modular_rate_law,
    "reversible mass action": rev_mass_action_rate_law,
    "irreversible mass action": irrev_mass_action_rate_law,
    "mass action": rev_mass_action_rate_law,
}

# TODO:
# convenience kinetics
# Linlog / loglin
# Hill type
# Michaelis menten


rate_law_sbo = {
    "default": "SBO:0000001",
    "modular": "SBO:0000527",
    "common modular": "SBO:0000528",
    "direct binding modular": "SBO:0000529",
    "force-dependent modular": "SBO:0000532",
    "power-law modular": "SBO:0000531",
    "simultaneous binding modular": "SBO:0000530",
    "reversible mass action": "SBO:0000042",
    "irreversible mass action": "SBO:0000041",
    "mass action": "SBO:0000012",
}


def generate_rate_laws(
    metabolites, reactions, stoichiometry, rate_law_type, **rate_law_args
):
    """Create a rate law term for a reaction.

    :param metabolites: List of metabolite names. (Same order as the stoichiometry.)
    :type metabolites: list[str]
    :param reactions: List of reaction names. (Same order as the stoichiometry.)
    :type reactions: list[str]
    :param stoichiometry: Stoichiometry matrix
    :type stoichiometry: (M, R) array
    :param rate_law_type: The type of rate law.
    :type rate_law_type: str
    :param rate_law_args: Extra arguments for the specific rate law.
    :type rate_law_args: dict

    :returns: A dictionairy of sympy expression for the rate laws.
    :rtype: dict[str: sympy expression]
    :returns: A list of parameters contained in those rate laws.
    :rtype: list[sympy expression]
    """
    try:
        rate_law_function = available_rate_laws[rate_law_type]
    except KeyError:
        raise ValueError(
            "Could not find a constuctor for: {}.\nChoose one of: {}".format(
                rate_law_type, ", ".join(available_rate_laws.keys())
            )
        )

    kinetics = {}
    parameters = set()
    for i, reaction in enumerate(reactions):
        row = stoichiometry[:, i]
        index = np.where(row != 0)[0]

        counts = [int(i) if int(i) == i else i for i in row[index]]
        metabolite_names = [metabolites[i] for i in index]
        mets = dict(zip(metabolite_names, counts))

        flux_term = rate_law_function(reaction, mets, **rate_law_args)
        kinetics[reaction] = flux_term
        parameters |= {i.name for i in flux_term.free_symbols}

    return kinetics, sorted(parameters - set(metabolites))
